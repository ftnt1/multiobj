#!/usr/bin/env python3

"""multiobj.py: Script finding policies using multiple objects in FortiOS config"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.0"
__status__ = "Production"

import os

def printnumlines(lst, s=0):
    for n, item in enumerate(lst):
        print(str(n+s) + ' - ' + str(item))


def hasvdoms():
    ans = 0
    for _ in cfg:
        if _ == 'config vdom\n':
            ans = 1
            break
    return ans

BLUE ='\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
NORM = '\033[0m'

# Clear the terminal
print(chr(27) + "[2J")

print(GREEN + '\n\n\n\n\nWelcome!\nThis is FortiOS MultiObject Policy Finder v1.0\n' + NORM)

# Open path
path = "./"
dirs = os.listdir(path)

# Create empty filelist
cfgfiles = []
cfgindex = 0

# This will add all .conf to list "cfgfiles"
for file in dirs:
    if ".conf" in file:
        cfgfiles.append(file)
        print(str(cfgindex) + " - " + file)
        cfgindex += 1
if cfgindex > 1:
    cfgindex = input("\nWhich file you want to open? Type number only! > ")
    cfgindex = int(cfgindex)
    cfgfile = cfgfiles[cfgindex]
else:
    print('\nOnly one config file found in current directory, I will use it')
    cfgfile = cfgfiles[0]
print('\nReading file: ' + cfgfile + '\n')
f = open(cfgfile)
cfg = f.readlines()
print('\nDone!\n')
f.close()
print('\nMultiObject policies in ' + cfgfile + ':\n')

# VDOMS
# slicing config file to start with 'config global\n' which will ease the vdom separation
confstartindex = 0
vdomstart = []
vdomend = []
vdlist = []
hasvd = hasvdoms()
if hasvd:
    for num, line in enumerate(cfg):
        if line == 'config global\n':
            confstartindex = num
    cfg = cfg[confstartindex:]

    for num, line in enumerate(cfg):
        if line == 'config vdom\n':
            vdomstart.append(num)

    for ind, startline in enumerate(vdomstart):
        for num, line in enumerate(cfg):
            if line == 'config vdom\n' and num > startline:
                vdomend.append(num)
                break
    vdomend.append(len(cfg))

    vdnum = range(len(vdomstart))
    obstart = []
    obend = []
    polstart = []
    polend = []
    grpstart = []
    grpend = []
    for vd in vdnum:
        checker = 0
        vdstart = vdomstart[vd]
        vdend = vdomend[vd]
        vdname = cfg[vdstart+1].strip(' edit').strip('\n')
        cfgslice = cfg[vdstart:vdend]
        vdlist.append(vdname)
        print('\n\n#####   VDOM: ' + vdname + '   #####\n')

    # OBJECTS
        for num, line in enumerate(cfgslice):
            if line == 'config firewall address\n':
                obstart.append(num)
                checker = num
                break
        if checker == 0:
            obstart.append(0)
            obend.append(0)
        else:
            for num, line in enumerate(cfgslice):
                if line == 'end\n' and num > obstart[vd]:
                    obend.append(num)
                    break
        obslice = cfgslice[obstart[vd]:obend[vd]]
        obtab = [line.strip(' edit ').strip('\n') for line in obslice if line.startswith('    edit ')]

    # POLICIES
        poltab = []
        checker = 0
        for num, line in enumerate(cfgslice):
            if line == 'config firewall policy\n':
                polstart.append(num)
                checker = num
                break
        if checker == 0:
            polstart.append(0)
            polend.append(0)
        else:
            for num, line in enumerate(cfgslice):
                if line == 'end\n' and num > polstart[vd]:
                    polend.append(num)
                    break
        polslice = cfgslice[polstart[vd]:polend[vd]]
        ids = []
        sources = []
        destinations = []
        for line in polslice:
            if line.startswith('    edit'):
                ids.append(line.strip(' edit ').strip('\n'))
            elif line.startswith('        set srcaddr '):
                sources.append(line.strip(' set srcadde ').strip('\n'))
            elif line.startswith('        set dstaddr '):
                destinations.append(line.strip(' set dstaddr ').strip('\n'))
        for number, line in enumerate(ids):
            poltab.append([ids[number], sources[number], destinations[number]])
        poltab = [[line[0], line[1].split(), line[2].split()] for line in poltab]
        for line in poltab:
            if len(line[1]) > 1 or len(line[2]) > 1:
                print(line[0])
