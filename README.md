# README #

Script finding policies using multiple objects in FortiOS config

## Files ##

### multiobj.py ###

This script prints all policy ID's containing multiple objects in 'srcaddr' or 'dstaddr' in their respective VDOM's'

Run it from within the directory containing firewall config(s).

It takes no command line arguments

### README.md ###

This file